# Ecofleet trial work
Live demo: http://128.199.58.9/

## Project structure
- doc/ input documentation
- src/ source code
  1. assets/ static content
  2. components/ Vue components
  3. util/ JavaScript utilities
  4. App.vue Vue base component
  5. index.html HTML template
  6. index.js Application entry point
  7. serverAPI.js Node server side script
- .babelrc Babel configuration
- .gitignore Git ignore
- package-lock.json NPM manifest
- package.json NPM configuration
- README.md readme, this document
- webpack.config.js Webpack configuration

## Project base libraries and tools
- NPM
- NodeCache
- Webpack
- Babel
- Vue.js
- OpenLayers
- Turf.js

## Setup
- Nodejs and NPM must be installed
```
$ git clone https://mihkelo@bitbucket.org/mihkelo/ecofleet.git
$ cd ecofleet
$ npm i
```
- For local development, start:
```
$ npm start
```
- For testing on server, start:
```
$ npm run start:prod
```
