import Vue from 'vue'
import App from './App.vue'

window.onload = function () {
  /**
   * Create Vue instance
   */
  /* eslint-disable-next-line no-new */
  new Vue({
    el: '#app',
    render: h => h(App)
  })
}
