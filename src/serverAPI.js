const axios = require('axios')
const convert = require('xml-js')
const NodeCache = require('node-cache')
const md5 = require('md5')

// set cache for static data
const memCache = new NodeCache({
  stdTTL: 60*24*10
})
// settings
const APIUrl = 'https://app.ecofleet.com/seeme/Api/Vehicles'
const convertOptions = {
  compact: true,
  trim: true,
  nativeType: true,
  nativeTypeAttributes: true,
  ignoreDeclaration: true
}
// ServerAPI class
class ServerAPI {

  /**
   * getLastData - fetch Ecofleet API data and pass it to client app
   *
   * @param {Object} req Request object
   *
   * @return {Promise}
   */
  getLastData (req) {
    return new Promise((resolve, reject) => {
      const query = req.query
      // check key
      if (!query || !query.key) {
        reject(new Error('Missing key'))
      }
      // fetch
      axios.get(APIUrl + '/getLastData', {
        params: query,
        responseType: 'text'
      })
      .then(function (response) {
        // xml to json
        const json = convert.xml2js(response.data, convertOptions)
        if (json && json.nodes && json.nodes.status && json.nodes.status._text === 0 && json.nodes.response) {
          const vehicles = json.nodes.response.node.map(vehicle => {
            // only needed attributes
            return {
              id: vehicle.objectId._text,
              name: vehicle.objectName._text,
              coordinates: [vehicle.longitude._text, vehicle.latitude._text],
              speed: vehicle.speed._text,
              timestamp: vehicle.timestamp._text
            }
          })
          // resolve
          resolve({
            status: 0,
            data: vehicles
          })
        } else {
          // handle errors
          if (json.nodes.errormessage) {
            reject(new Error(json.nodes.errormessage._text))
          } else {
            reject(new Error('Unknown error'))
          }
        }
      })
      .catch(function (error) {
        reject(error)
      })
    })
  }

  /**
   * getRawData - fetch Ecofleet API data and pass it to client app
   *
   * @param {Object} req Request object
   *
   * @return {Promise}
   */
  getRawData (req) {
    return new Promise((resolve, reject) => {
      const query = req.query
      // check params
      if (!query || !query.key || !query.objectId || !query.begTimestamp || !query.endTimestamp) {
        reject(new Error('Missing request parameters'))
      }
      // check cache
      const hash = md5(JSON.stringify(query))
      const cacheItem = memCache.get(hash)
      if (cacheItem !== undefined ){
        // send from cache
        resolve(cacheItem)
      } else {
        // make request if not found from cache
        axios.get(APIUrl + '/getRawData', {
          params: query,
          responseType: 'text'
        })
        .then(function (response) {
          // xml to json
          const json = convert.xml2js(response.data, convertOptions)
          if (json && json.nodes.status._text === 0 && json.nodes.response.node && json.nodes.response.node.length) {
            const coordinates = json.nodes.response.node.map(location => {
              return [location.Longitude._text, location.Latitude._text]
            })
            const data = {
              status: 0,
              objectId: query.objectId,
              data: coordinates
            }
            // set cache
            memCache.set(hash, data)
            resolve(data)
          } else {
            // handle errors
            if (json.nodes.errormessage) {
              reject(new Error(json.nodes.errormessage._text))
            } else {
              reject(new Error('Unknown error'))
            }
          }
        })
        .catch(function (error) {
          reject(error)
        })
      }
    })
  }
}
module.exports = new ServerAPI()
