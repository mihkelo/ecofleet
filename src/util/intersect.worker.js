import intersect from '@turf/intersect'
import area from '@turf/area'

// listen messages from parent
self.addEventListener('message', (event) => {
  const tracks = event.data.tracks
  if (tracks) {
    const intersected = calculateIntersections(event.data.feature, tracks)
    sendResponse({
      id: event.data.feature.id,
      result: intersected
    })
  }
})

/**
 * calculateIntersections - find intersection geometries and calculate intersected amounts
 *
 * @param {Object} feature GeoJSON feature
 * @param {Array} tracks  Array of GeoJSON features
 *
 * @return {Object}
 */
function calculateIntersections (feature, tracks) {
  // reference area
  const area1 = area(feature.geometry)
  const row = {}
  tracks.forEach(track => {
    // skip same feature
    if (feature.id === track.id) {
      row[track.id] = {
        amount: 100,
        geom: track.geometry
      }
    } else {
      // intersection
      const overlap = intersect(feature.geometry, track.geometry)
      // intersection area
      const area2 = area(overlap.geometry)
      const percent = (area2/area1)*100
      row[track.id] = {
        amount: (Math.round(percent * 100) / 100),
        geom: overlap.geometry
      }
    }
  })
  return row
}

/**
 * sendResponse - Send response to parent thread
 *
 * @param {Object} data Data to send
 */
function sendResponse (data) {
  self.postMessage(data)
}
