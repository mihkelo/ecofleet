const webpack = require('webpack')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const server = require('./src/serverAPI.js')

module.exports = {
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'file-loader'
      },
      {
        test: /\.worker\.js$/,
        use: { loader: 'worker-loader' }
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,
    before: function (app) {
      app.get('/getLastData', (req, res) => {
        server.getLastData(req)
          .then(json => {
            res.end(JSON.stringify(json))
          })
          .catch(e => {
            res.end(JSON.stringify({status: -1, msg: e.message}))
          })
      })
      app.get('/getRawData', (req, res) => {
        server.getRawData(req)
          .then(json => {
            res.end(JSON.stringify(json))
          })
          .catch(e => {
            res.end(JSON.stringify({status: -1, msg: e.message}))
          })
      })
    }
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'src/index.html',
      inject: true
    })
  ]
}
